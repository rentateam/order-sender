<?php
namespace Astartsky\OrderSender\Call;

class MakeOrderRespond implements RespondInterface
{
    protected $trackUrl;
    protected $orderId;
    protected $status;
    protected $error;

    /**
     * @param int $orderId
     */
    protected function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param string $status
     */
    protected function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $trackUrl
     */
    protected function setTrackUrl($trackUrl)
    {
        $this->trackUrl = $trackUrl;
    }

    /**
     * @return string
     */
    public function getTrackUrl()
    {
        return $this->trackUrl;
    }

    /**
     * @param string $error
     */
    protected function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    public static function create(\SimpleXMLElement $xml)
    {
        $orderCallResult = new MakeOrderRespond();
        if ($xml->error) {
            $orderCallResult->setError($xml->error);
        }
        if ($xml->track_url) {
            $orderCallResult->setTrackUrl($xml->track_url);
        }
        if ($xml->status) {
            $orderCallResult->setStatus($xml->status);
        }
        if ($xml->order_id) {
            $orderCallResult->setOrderId($xml->order_id);
        }

        return $orderCallResult;
    }
}