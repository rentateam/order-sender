<?php
namespace Astartsky\OrderSender\Call;

interface RespondInterface
{
    /**
     * @return string
     */
    public function getError();
}